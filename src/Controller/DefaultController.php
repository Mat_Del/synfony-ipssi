<?php

namespace App\Controller;


use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
         //Get the doctrine manager
         $em = $this->getDoctrine()->getManager();
        
         //Get all entities
         $articles = $em->getRepository(Article:: class)->findAll();
 
         return $this->render('default/index.html.twig', [
             'controller_name' => 'DefaultController',
             'articles'=> $articles,
         ]);
    }
}
