<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ArticleType;

class ArticleController extends AbstractController
{
    /**
     * @Route("/article", name="article")
     */
    public function index()
    {

        //Get the doctrine manager
        $em = $this->getDoctrine()->getManager();
        
        //Get all entities
        $articles = $em->getRepository(Article:: class)->findAll();

        return $this->render('article/index.html.twig', [
            'controller_name' => 'ArticleController',
            'articles'=> $articles,
        ]);
    }

    /**
     * @Route("/article/show/{id}", name="show")
     */
    public function show($id)
    {

        //Get the doctrine manager
        $em = $this->getDoctrine()->getManager();
        
        //Get all entities
        $articles = $em->getRepository(Article:: class)->find($id);

        return $this->render('article/show.html.twig', [
            'controller_name' => 'ArticleController',
            'articles'=> $articles,
        ]);
    }

    

    /**
     * @Route("/article/{id}/edit", name="edit_article")
     * @Route("/article/upload", name="upload_article")
     */
    public function upload(Request $request, Article $article=NULL)
    {
        if (!$article)
        {
            $article = new Article();
        }

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $published = new \DateTime();
            $online = 1;
            $article->setPublished($published);
            $article->setOnline($online);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();
            $this->addFlash('notice', 'Article crée');
            $this->redirectToRoute('article');
        }

        return $this->render('article/upload.html.twig', [
            'controller_name' => 'ArticleController',
            'form' => $form->createView(),
            'notEmptyForm' => $article->getId() !== NULL
        ]);
    }

    /**
     * @Route("/article/{id}/delete", name="delete_article")
     */
    public function delete(Request $request, Article $article=NULL)
    {
        
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($article);
        $entityManager->remove($article);
        $entityManager->flush();
        $this->addFlash('notice', 'Article supprimé');
        $this->redirectToRoute('article');

        return $this->render('article/delete.html.twig', [
            'controller_name' => 'ArticleController',
        ]);
    }

}
